package ro.tuc.ds2020.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.MedTakenDTO;
import ro.tuc.ds2020.dtos.MedicationPlanDTO;
import ro.tuc.ds2020.services.MedTakenService;
import ro.tuc.ds2020.services.MedicationPlanService;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@CrossOrigin
@RequestMapping(value = "/medTaken")
public class MedTakenController {

    private final MedTakenService medTakenService;

    @Autowired
    public MedTakenController(MedTakenService medTakenService) {

        this.medTakenService = medTakenService;
    }

    @GetMapping("/allMedTaken")
    public ResponseEntity<List<MedTakenDTO>> getMedTaken() {
        List<MedTakenDTO> dtos = medTakenService.findMedTaken();
        for (MedTakenDTO dto : dtos) {
            Link medTakenLink = linkTo(methodOn(MedTakenController.class)
                    .getMedTaken()).withRel("medicationPlanDetails");
            dto.add(medTakenLink);
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @PostMapping("/insert")
    public ResponseEntity<UUID> insertMedTaken(@Valid @RequestBody MedTakenDTO medTakenDTO) {
        UUID medTakenID = medTakenService.insert(medTakenDTO);
        return new ResponseEntity<>(medTakenID, HttpStatus.CREATED);
    }

}

