package ro.tuc.ds2020.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.CaregiverDTO;
import ro.tuc.ds2020.dtos.DoctorDTO;
import ro.tuc.ds2020.dtos.PatientDTO;
import ro.tuc.ds2020.services.CaregiverService;
import ro.tuc.ds2020.services.DoctorService;
import ro.tuc.ds2020.services.PatientService;

import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/login")
public class LoginController {

    private final CaregiverService caregiverService;
    private final PatientService patientService;
    private final DoctorService doctorService;

    @Autowired
    public LoginController(CaregiverService caregiverService, PatientService patientService, DoctorService doctorService) {

        this.caregiverService = caregiverService;
        this.patientService = patientService;
        this.doctorService=doctorService;
    }

    @GetMapping()
    public ResponseEntity<UUID> login(@RequestParam(value="email") String email, @RequestParam(value="password")String password){
        DoctorDTO doctor=new DoctorDTO();
        PatientDTO patient=new PatientDTO();
        CaregiverDTO caregiver=new CaregiverDTO();
        int wrong = 0;
        System.out.println("@@@@@@@@@@@@@22");

        patient=patientService.findPatientByEmail(email);
        caregiver=caregiverService.findCaregiverByEmail(email);
        doctor=doctorService.findDoctorByEmail(email);


        if(email.equals("")) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }

        if(password.equals("")) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }

        if(doctor == null && patient == null && caregiver==null){
            wrong++;
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }

        if(wrong == 0){
            if(doctor != null && email.equals(doctor.getEmail())) {
                if(password.equals(doctor.getPassword())) {
                    return new ResponseEntity<>(doctor.getIdDoctor(), HttpStatus.ACCEPTED);

                }
                else {
                    wrong++;
                }
            }
            else if(patient != null){
                if(email.equals(patient.getEmail())){
                    if(password.equals(patient.getPassword())) {
                        return new ResponseEntity<>(patient.getId(), HttpStatus.ACCEPTED);

                    }else{
                        wrong++;
                    }
                }
                else {
                    wrong++;
                }
            }else if(caregiver!= null){
                if(email.equals(caregiver.getEmail())){
                    if(password.equals(caregiver.getPassword())) {
                        return new ResponseEntity<>(caregiver.getId_caregiver(), HttpStatus.ACCEPTED);

                    }else{
                        wrong++;
                    }
                }
                else {
                    wrong++;
                }
            }
        }

        return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
    }

}