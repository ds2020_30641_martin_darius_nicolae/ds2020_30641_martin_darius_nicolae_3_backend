package ro.tuc.ds2020.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.MedicationDTO;
import ro.tuc.ds2020.dtos.PatientDTO;
import ro.tuc.ds2020.services.MedicationService;


import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@CrossOrigin
@RequestMapping(value = "/medication")
public class MedicationController {

    private final MedicationService medicationService;

    @Autowired
    public MedicationController(MedicationService medicationService) {

        this.medicationService = medicationService;
    }

    @GetMapping("/allMedications")
    public ResponseEntity<List<MedicationDTO>> getMedication() {
        List<MedicationDTO> dtos = medicationService.findMedications();
        for (MedicationDTO dto : dtos) {
            Link medicationLink = linkTo(methodOn(MedicationController.class)
                    .getMedication(dto.getId_medication())).withRel("medicationDetails");
            dto.add(medicationLink);
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @PostMapping("/insert")
    public ResponseEntity<UUID> insertMedication(@Valid @RequestBody MedicationDTO medicationDTO) {
        UUID medicationID = medicationService.insert(medicationDTO);
        return new ResponseEntity<>(medicationID, HttpStatus.CREATED);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<MedicationDTO> getMedication(@PathVariable("id") UUID medicationId) {
        MedicationDTO dto = medicationService.findMedicationById(medicationId);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    //TODO: UPDATE, DELETE per resource

    @DeleteMapping("/delete/{name}")
    public ResponseEntity<UUID> deleteMedication(@PathVariable("name") String medicationName) {
        UUID id=medicationService.delete(medicationName);
        return new ResponseEntity<>(id, HttpStatus.OK);
    }

    @PutMapping("/update/{name}")
    public ResponseEntity<UUID> updateMedication(@PathVariable("name") String medicationName,@Valid @RequestBody MedicationDTO updatedMedication) {
        MedicationDTO medicationDTO=medicationService.findMedicationByName(medicationName);
        medicationDTO=medicationService.findMedicationById(medicationDTO.getId_medication());
        medicationDTO.setName(updatedMedication.getName());
        medicationDTO.setDosage(updatedMedication.getDosage());
        medicationDTO.setSideEffects(updatedMedication.getSideEffects());
        UUID id=medicationService.update(medicationDTO);
        return new ResponseEntity<>(id, HttpStatus.OK);
    }

    @GetMapping("/getMedication/{name}")
    public ResponseEntity<List<MedicationDTO>> getMedicationByName(@PathVariable("name") String medicationList) {

        String[] parts = medicationList.split(",");
        List<MedicationDTO> meds= new ArrayList<MedicationDTO>();
        for(String part: parts){
            System.out.println(part+"@@@@@@@@@");
            MedicationDTO medication=medicationService.findMedicationByName(part);
            meds.add(medication);
        }
        return new ResponseEntity<>(meds, HttpStatus.OK);
    }

}
