package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.PatientDTO;
import ro.tuc.ds2020.dtos.PersonDTO;
import ro.tuc.ds2020.dtos.PersonDetailsDTO;
import ro.tuc.ds2020.dtos.builders.PatientBuilder;
import ro.tuc.ds2020.dtos.builders.PersonBuilder;
import ro.tuc.ds2020.entities.Patient;
import ro.tuc.ds2020.entities.Person;
import ro.tuc.ds2020.repositories.PatientRepository;
import ro.tuc.ds2020.repositories.PersonRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class PatientService {
    private static final Logger LOGGER = LoggerFactory.getLogger(PatientService.class);
    private final PatientRepository patientRepository;

    @Autowired
    public PatientService(PatientRepository patientRepository) {
        this.patientRepository = patientRepository;
    }

    public List<PatientDTO> findPatients() {
        List<Patient> patientList = patientRepository.findAll();
        return patientList.stream()
                .map(PatientBuilder::toPatientDTO)
                .collect(Collectors.toList());
    }

    public PatientDTO findPatientById(UUID id) {
        Optional<Patient> prosumerOptional = patientRepository.findById(id);
        if (!prosumerOptional.isPresent()) {
            LOGGER.error("Patient with id {} was not found in db", id);
            throw new ResourceNotFoundException(Patient.class.getSimpleName() + " with id: " + id);
        }
        return PatientBuilder.toPatientDTO(prosumerOptional.get());
    }

    public PatientDTO findPatientByName(String name) {
        List<Patient> list = patientRepository.findByName(name);
        if (list.isEmpty()) {
            LOGGER.error("Patient with name {} was not found in db", name);
            throw new ResourceNotFoundException(Patient.class.getSimpleName() + " with name: " + name);
        }
        return PatientBuilder.toPatientDTO(list.get(0));
    }

    public UUID insert(PatientDTO patientDTO) {
        System.out.println(patientDTO.getEmail()+"@@@@@@@");
        Patient patient = PatientBuilder.toEntity(patientDTO);
        patient = patientRepository.save(patient);
        LOGGER.debug("Person with id {} was inserted in db", patient.getId());
        return patient.getId();
    }

    public UUID delete(String name){
        List<Patient>  listOfPatients=patientRepository.findByName(name);
        patientRepository.delete(listOfPatients.get(0));
        return listOfPatients.get(0).getId();
    }

    public UUID update(PatientDTO updatedPatient){
        Patient patient = PatientBuilder.transform(updatedPatient);
        patient = patientRepository.save(patient);
        LOGGER.debug("Patient with id {} was updated in db", patient.getId());
        return patient.getId();
    }


    public PatientDTO findPatientByEmail(String email){
        List<PatientDTO> patients=this.findPatients();
        if(patients!=null) {
            for (PatientDTO patient : patients) {
                if (patient.getEmail().equals(email)){
                    return patient;
                }
            }
        }
        return null;
    }
}