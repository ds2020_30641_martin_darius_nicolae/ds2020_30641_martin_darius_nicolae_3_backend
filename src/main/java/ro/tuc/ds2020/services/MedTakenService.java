package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.dtos.MedTakenDTO;
import ro.tuc.ds2020.dtos.MedicationDTO;
import ro.tuc.ds2020.dtos.builders.MedTakenBuilder;
import ro.tuc.ds2020.dtos.builders.MedicationBuilder;
import ro.tuc.ds2020.entities.MedTaken;
import ro.tuc.ds2020.entities.Medication;
import ro.tuc.ds2020.repositories.MedTakenRepository;
import ro.tuc.ds2020.repositories.MedicationRepository;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class MedTakenService {
    private static final Logger LOGGER = LoggerFactory.getLogger(MedTakenService.class);
    private final MedTakenRepository medTakenRepository;

    @Autowired
    public MedTakenService(MedTakenRepository medTakenRepository) {
        this.medTakenRepository = medTakenRepository;
    }

    public List<MedTakenDTO> findMedTaken() {
        List<MedTaken> medicationList = medTakenRepository.findAll();
        return medicationList.stream()
                .map(MedTakenBuilder::toMedTakenDTO)
                .collect(Collectors.toList());
    }

    public UUID insert(MedTakenDTO medTakenDTO) {
        MedTaken medTaken = MedTakenBuilder.toEntity(medTakenDTO);
        medTaken = medTakenRepository.save(medTaken);
        LOGGER.debug("Medication with id {} was inserted in db", medTaken.getId_medtaken());
        return medTaken.getId_medtaken();
    }

}