package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.CaregiverDTO;
import ro.tuc.ds2020.dtos.MedicationDTO;
import ro.tuc.ds2020.dtos.MedicationPlanDTO;
import ro.tuc.ds2020.dtos.builders.CaregiverBuilder;
import ro.tuc.ds2020.dtos.builders.MedicationBuilder;
import ro.tuc.ds2020.dtos.builders.MedicationPlanBuilder;
import ro.tuc.ds2020.entities.Caregiver;
import ro.tuc.ds2020.entities.Medication;
import ro.tuc.ds2020.entities.MedicationPlan;
import ro.tuc.ds2020.repositories.MedicationPlanRepository;
import ro.tuc.ds2020.repositories.MedicationRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class MedicationPlanService {
    private static final Logger LOGGER = LoggerFactory.getLogger(MedicationPlanService.class);
    private final MedicationPlanRepository medicationPlanRepository;

    @Autowired
    public MedicationPlanService(MedicationPlanRepository medicationPlanRepository) {
        this.medicationPlanRepository = medicationPlanRepository;
    }

    public List<MedicationPlanDTO> findMedicationPlans() {
        List<MedicationPlan> medicationPlanList = medicationPlanRepository.findAll();
        return medicationPlanList.stream()
                .map(MedicationPlanBuilder::toMedicationPlanDTO)
                .collect(Collectors.toList());
    }

    public MedicationPlanDTO findMedicationPlanById(UUID id) {
        Optional<MedicationPlan> medicationPlan = medicationPlanRepository.findById(id);
        if (!medicationPlan.isPresent()) {
            LOGGER.error("MedicationPlan with id {} was not found in db", id);
            throw new ResourceNotFoundException(MedicationPlan.class.getSimpleName() + " with id: " + id);
        }
        return MedicationPlanBuilder.toMedicationPlanDTO(medicationPlan.get());
    }

    public MedicationPlanDTO findMedicationPlanByName(String name) {
        List<MedicationPlan> list = medicationPlanRepository.findByName(name);
        if (list.isEmpty()) {
            LOGGER.error("MedicationPlan with name {} was not found in db", name);
            throw new ResourceNotFoundException(MedicationPlan.class.getSimpleName() + " with name: " + name);
        }
        return MedicationPlanBuilder.toMedicationPlanDTO(list.get(0));
    }


    public UUID insert(MedicationPlanDTO medicationPlanDTO) {
        MedicationPlan medicationPlan = MedicationPlanBuilder.toEntity(medicationPlanDTO);
        medicationPlan = medicationPlanRepository.save(medicationPlan);
        LOGGER.debug("MedicationPlan with id {} was inserted in db", medicationPlan.getId_medicationPlan());
        return medicationPlan.getId_medicationPlan();
    }

    public UUID delete(String name){
        List<MedicationPlan>  listOfMedicationPlans=medicationPlanRepository.findByName(name);
        medicationPlanRepository.delete(listOfMedicationPlans.get(0));
        return listOfMedicationPlans.get(0).getId_medicationPlan();
    }

    public UUID update(MedicationPlanDTO updateMedicationPlan){
        MedicationPlan medicationPlan = MedicationPlanBuilder.transform(updateMedicationPlan);
        medicationPlan = medicationPlanRepository.save(medicationPlan);
        LOGGER.debug("MedicationPlan with id {} was updated in db", medicationPlan.getId_medicationPlan());
        return medicationPlan.getId_medicationPlan();
    }

}

