package ro.tuc.ds2020.rpc;

import org.springframework.beans.factory.annotation.Autowired;
import ro.tuc.ds2020.dtos.MedTakenDTO;
import ro.tuc.ds2020.dtos.MedicationPlanDTO;
import ro.tuc.ds2020.entities.MedTaken;
import ro.tuc.ds2020.services.MedTakenService;
import ro.tuc.ds2020.services.MedicationPlanService;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class TakeMedicationServiceImpl implements TakeMedicationService {

    private MedicationPlanService medicationPlanService;
    private MedTakenService medTakenService;

    @Autowired
    public TakeMedicationServiceImpl(MedicationPlanService medicationPlanService, MedTakenService medTakenService){
        this.medicationPlanService=medicationPlanService;
        this.medTakenService=medTakenService;
    }

    @Override
    public List<MedicationPlanDTO> takeAMed(UUID patientID) throws TakingExceptions {
        List<MedicationPlanDTO> medicationPlans=medicationPlanService.findMedicationPlans();
        List<MedicationPlanDTO> list=new ArrayList<>();
        for(MedicationPlanDTO medicationPlan: medicationPlans){
            if(medicationPlan.getPatientDTO().getId().equals(patientID))
                list.add(medicationPlan);
        }
        return list;
    }

    @Override
    public String take(MedTakenDTO medTaken) throws TakingExceptions{
        UUID response=medTakenService.insert(medTaken);
        //return response.toString();
        return "Salut";
    }
}
