package ro.tuc.ds2020.rpc;

public class TakingExceptions extends Exception {
    public TakingExceptions(String message){
        super(message);
    }
}
