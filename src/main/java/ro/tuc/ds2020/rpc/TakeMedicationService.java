package ro.tuc.ds2020.rpc;

import ro.tuc.ds2020.dtos.MedTakenDTO;
import ro.tuc.ds2020.dtos.MedicationPlanDTO;
import ro.tuc.ds2020.entities.MedTaken;

import java.util.List;
import java.util.UUID;

public interface TakeMedicationService {

     List<MedicationPlanDTO> takeAMed(UUID patientID) throws TakingExceptions;
     String take(MedTakenDTO medTaken) throws TakingExceptions;
}
