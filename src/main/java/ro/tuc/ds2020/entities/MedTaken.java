package ro.tuc.ds2020.entities;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.UUID;

@Entity
public class MedTaken  implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Type(type = "uuid-binary")
    private UUID id_medtaken;

    @Column(name = "medicationPlan", nullable = false)
    private UUID medicationPlan;

    @Column(name = "medication", nullable = false)
    private UUID medication;

    @Column(name = "patient", nullable = false)
    private UUID patient;

    @Column(name = "taken", nullable = false)
    private boolean taken;

    public MedTaken(){

    }
    public MedTaken( UUID medicationPlan, UUID medication, UUID patient, boolean taken) {
        this.medicationPlan = medicationPlan;
        this.medication = medication;
        this.patient = patient;
        this.taken = taken;
    }

    public MedTaken(UUID id_medtaken, UUID medicationPlan, UUID medication, UUID patient, boolean taken) {
        this.id_medtaken = id_medtaken;
        this.medicationPlan = medicationPlan;
        this.medication = medication;
        this.patient = patient;
        this.taken = taken;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public UUID getId_medtaken() {
        return id_medtaken;
    }

    public void setId_medtaken(UUID id_medtaken) {
        this.id_medtaken = id_medtaken;
    }

    public UUID getMedicationPlan() {
        return medicationPlan;
    }

    public void setMedicationPlan(UUID medicationPlan) {
        this.medicationPlan = medicationPlan;
    }

    public UUID getMedication() {
        return medication;
    }

    public void setMedication(UUID medication) {
        this.medication = medication;
    }

    public UUID getPatient() {
        return patient;
    }

    public void setPatient(UUID patient) {
        this.patient = patient;
    }

    public boolean isTaken() {
        return taken;
    }

    public void setTaken(boolean taken) {
        this.taken = taken;
    }
}