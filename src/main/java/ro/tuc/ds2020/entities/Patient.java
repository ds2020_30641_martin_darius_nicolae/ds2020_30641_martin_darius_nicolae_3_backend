package ro.tuc.ds2020.entities;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;
import java.util.UUID;

@Entity
public class Patient  implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Type(type = "uuid-binary")
    private UUID id_patient;


    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "birthDate", nullable = false)
    private Date birthDate;

    @Column(name = "gender", nullable = false)
    private String gender;

    @Column(name = "email", nullable = false)
    private String email;

    @Column(name = "password", nullable = false)
    private String password;


    @Column(name = "address", nullable = false)
    private String address;

    @Column(name = "medicalRecord", nullable = false)
    private String medicalRecord;

    @ManyToOne(fetch = FetchType.EAGER,cascade=CascadeType.DETACH)
    @JoinColumn(name = "id_caregiver")
    private Caregiver caregiver;


    public Patient() {
    }

    public Patient(String name,Date birthDate,String gender, String address, String medicalRecord,String email, String password, Caregiver caregiver) {
        this.name = name;
        this.birthDate=birthDate;
        this.gender=gender;
        this.address = address;
        this.medicalRecord = medicalRecord;
        this.email=email;
        this.password=password;
        this.caregiver=caregiver;
    }

    public Patient(UUID id,String name,Date birthDate,String gender, String address, String medicalRecord,String email, String password,Caregiver caregiver) {
        this.id_patient=id;
        this.name = name;
        this.birthDate=birthDate;
        this.gender=gender;
        this.address = address;
        this.medicalRecord = medicalRecord;
        this.email=email;
        this.password=password;
        this.caregiver=caregiver;
    }

    public UUID getId() {
        return id_patient;
    }

    public void setId(UUID id) {
        this.id_patient = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getMedicalRecord() {
        return medicalRecord;
    }

    public void setMedicalRecord(String medicalRecord) {
        this.medicalRecord = medicalRecord;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Caregiver getCaregiver() {
        return caregiver;
    }

    public void setCaregiver(Caregiver caregiver) {
        this.caregiver = caregiver;
    }
}