package ro.tuc.ds2020;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.remoting.httpinvoker.HttpInvokerProxyFactoryBean;
import org.springframework.remoting.httpinvoker.HttpInvokerServiceExporter;
import org.springframework.validation.annotation.Validated;
import ro.tuc.ds2020.rpc.TakeMedicationService;
import ro.tuc.ds2020.rpc.TakeMedicationServiceImpl;
import ro.tuc.ds2020.services.MedTakenService;
import ro.tuc.ds2020.services.MedicationPlanService;

import java.util.TimeZone;

@SpringBootApplication
@Validated
public class Ds2020Application extends SpringBootServletInitializer {

    @Autowired
    private MedicationPlanService medicationPlanService;
    @Autowired
    private MedTakenService medTakenService;

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(Ds2020Application.class);
    }

    public static void main(String[] args) {
		TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
        SpringApplication.run(Ds2020Application.class, args);
    }

    @Bean(name = "/taking")
    HttpInvokerServiceExporter accountService() {
        HttpInvokerServiceExporter exporter = new HttpInvokerServiceExporter();
        exporter.setService( new TakeMedicationServiceImpl(medicationPlanService,medTakenService) );
        exporter.setServiceInterface( TakeMedicationService.class );
        return exporter;
    }
}
