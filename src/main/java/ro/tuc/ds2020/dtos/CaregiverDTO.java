package ro.tuc.ds2020.dtos;

import org.springframework.hateoas.RepresentationModel;
import ro.tuc.ds2020.entities.Patient;

import java.io.Serializable;
import java.sql.Date;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

public class CaregiverDTO extends RepresentationModel<CaregiverDTO> implements Serializable {
    private static final long serialVersionUID = 1L;
    private UUID id_caregiver;
    private String name;
    private Date birthDate;
    private String address;
    private String gender;
    private String email;
    private String password;


    public CaregiverDTO(){

    }

    public CaregiverDTO(UUID id_caregiver,String name, Date birthDate, String address, String gender,String email,String password) {
        this.id_caregiver=id_caregiver;
        this.name = name;
        this.birthDate = birthDate;
        this.address = address;
        this.gender = gender;
        this.email=email;
        this.password=password;

    }

    public CaregiverDTO(String name, Date birthDate, String address, String gender,String email,String password) {
        this.name = name;
        this.birthDate = birthDate;
        this.address = address;
        this.gender = gender;
        this.email=email;
        this.password=password;

    }

    public UUID getId_caregiver() {
        return id_caregiver;
    }

    public void setId_caregiver(UUID id_caregiver) {
        this.id_caregiver = id_caregiver;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        CaregiverDTO that = (CaregiverDTO) o;
        return id_caregiver.equals(that.id_caregiver) &&
                name.equals(that.name) &&
                birthDate.equals(that.birthDate) &&
                address.equals(that.address) &&
                gender.equals(that.gender) &&
                email.equals(that.email) &&
                password.equals(that.password);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), id_caregiver, name, birthDate, address, gender, email, password);
    }
}
