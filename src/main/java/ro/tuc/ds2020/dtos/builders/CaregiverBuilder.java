package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.CaregiverDTO;
import ro.tuc.ds2020.dtos.PatientDTO;
import ro.tuc.ds2020.dtos.PersonDTO;
import ro.tuc.ds2020.dtos.PersonDetailsDTO;
import ro.tuc.ds2020.entities.Caregiver;
import ro.tuc.ds2020.entities.Patient;
import ro.tuc.ds2020.entities.Person;

public class CaregiverBuilder {

    private CaregiverBuilder() {
    }

    public static CaregiverDTO toCaregiverDTO(Caregiver caregiver) {
        return new CaregiverDTO(caregiver.getId(),caregiver.getName(),caregiver.getBirthDate(),caregiver.getAddress(),caregiver.getGender(),caregiver.getEmail(), caregiver.getPassword());
    }


    public static Caregiver toEntity(CaregiverDTO caregiverDTO) {
        return new Caregiver(caregiverDTO.getName(),caregiverDTO.getBirthDate(),caregiverDTO.getGender(),caregiverDTO.getAddress(), caregiverDTO.getEmail(), caregiverDTO.getPassword());
    }

    public static Caregiver transform(CaregiverDTO caregiverDTO) {
        return new Caregiver(caregiverDTO.getId_caregiver(), caregiverDTO.getName(), caregiverDTO.getBirthDate(), caregiverDTO.getGender(), caregiverDTO.getAddress(), caregiverDTO.getEmail(), caregiverDTO.getPassword());
    }
}
