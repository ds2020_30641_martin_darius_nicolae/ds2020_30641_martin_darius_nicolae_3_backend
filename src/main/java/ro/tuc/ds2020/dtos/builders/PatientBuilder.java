package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.CaregiverDTO;
import ro.tuc.ds2020.dtos.PatientDTO;
import ro.tuc.ds2020.dtos.PersonDTO;
import ro.tuc.ds2020.dtos.PersonDetailsDTO;
import ro.tuc.ds2020.entities.Caregiver;
import ro.tuc.ds2020.entities.Patient;
import ro.tuc.ds2020.entities.Person;

public class PatientBuilder {

    private PatientBuilder() {
    }

    public static PatientDTO toPatientDTO(Patient patient) {
        CaregiverDTO caregiverDTO=null;
        if(patient.getCaregiver()!=null) {
            caregiverDTO=CaregiverBuilder.toCaregiverDTO(patient.getCaregiver());
        }
         return new PatientDTO(patient.getId(),patient.getName(),patient.getBirthDate(),patient.getAddress(),patient.getGender(),patient.getMedicalRecord(),patient.getEmail(), patient.getPassword(),caregiverDTO);
    }


    public static Patient toEntity(PatientDTO patientDTO) {
        Caregiver caregiver=null;
        if(patientDTO.getCaregiver()!=null){
            caregiver=CaregiverBuilder.transform(patientDTO.getCaregiver());
        }
        return new Patient(patientDTO.getName(),patientDTO.getBirthDate(),patientDTO.getGender(),patientDTO.getAddress(),patientDTO.getMedicalRecord(), patientDTO.getEmail(), patientDTO.getPassword(),caregiver);
    }

    public static Patient transform(PatientDTO patientDTO) {
        Caregiver caregiver=null;
        if(patientDTO.getCaregiver()!=null){
            caregiver=CaregiverBuilder.transform(patientDTO.getCaregiver());
        }
        return new Patient(patientDTO.getId(), patientDTO.getName(), patientDTO.getBirthDate(), patientDTO.getGender(), patientDTO.getAddress(), patientDTO.getMedicalRecord(), patientDTO.getEmail(), patientDTO.getPassword(),caregiver);
    }
}
