package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.MedTakenDTO;
import ro.tuc.ds2020.dtos.MedicationDTO;
import ro.tuc.ds2020.entities.MedTaken;
import ro.tuc.ds2020.entities.Medication;

public class MedTakenBuilder {

    private MedTakenBuilder() {
    }

    public static MedTakenDTO toMedTakenDTO(MedTaken medTaken) {
        return new MedTakenDTO(medTaken.getId_medtaken(),medTaken.getMedicationPlan(),medTaken.getMedication(),medTaken.getPatient(),medTaken.isTaken());
    }


    public static MedTaken toEntity(MedTakenDTO medTakenDTO) {
        return new MedTaken(medTakenDTO.getMedicationPlan(),medTakenDTO.getMedication(),medTakenDTO.getPatient(),medTakenDTO.isTaken());
    }

    public static MedTaken transform(MedTakenDTO medTakenDTO) {
        return new MedTaken(medTakenDTO.getId_medtaken(),medTakenDTO.getMedicationPlan(),medTakenDTO.getMedication(),medTakenDTO.getPatient(),medTakenDTO.isTaken());
    }
}
