package ro.tuc.ds2020.services;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.jdbc.Sql;
import ro.tuc.ds2020.Ds2020TestConfig;
import ro.tuc.ds2020.dtos.MedicationDTO;
import ro.tuc.ds2020.dtos.PersonDTO;
import ro.tuc.ds2020.dtos.PersonDetailsDTO;

import static org.springframework.test.util.AssertionErrors.assertEquals;

import java.util.List;
import java.util.UUID;

@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:/test-sql/create.sql")
@Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = "classpath:/test-sql/delete.sql")
public class MedicationServiceIntegrationTests extends Ds2020TestConfig {

    @Autowired
    MedicationService medicationService;

    @Test
    public void testGetCorrect() {
        List<MedicationDTO> medicationDTOList = medicationService.findMedications();
        assertEquals("Test Insert Medication", 2, medicationDTOList.size());
    }

    @Test
    public void testInsertCorrectWithGetById() {
        MedicationDTO p = new MedicationDTO("paracetamol", "febra", "22");
        UUID insertedID = medicationService.insert(p);

        MedicationDTO insertedMedication = new MedicationDTO(insertedID, p.getName(),p.getSideEffects(), p.getDosage());
        MedicationDTO fetchedMedication = medicationService.findMedicationById(insertedID);

        assertEquals("Test Inserted Medication", insertedMedication, fetchedMedication);
    }

    @Test
    public void testInsertCorrectWithGetAll() {
        MedicationDTO p = new MedicationDTO("nurofen", "mancarimi", "10");
        medicationService.insert(p);

        List<MedicationDTO> medicationDTOList = medicationService.findMedications();
        assertEquals("Test Inserted Medication", 1, medicationDTOList.size());
    }
}
