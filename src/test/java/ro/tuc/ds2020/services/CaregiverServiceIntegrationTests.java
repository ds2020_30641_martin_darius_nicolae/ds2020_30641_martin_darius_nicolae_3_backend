package ro.tuc.ds2020.services;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.jdbc.Sql;
import ro.tuc.ds2020.Ds2020TestConfig;
import ro.tuc.ds2020.dtos.CaregiverDTO;
import ro.tuc.ds2020.dtos.MedicationDTO;
import ro.tuc.ds2020.dtos.PersonDTO;
import ro.tuc.ds2020.dtos.PersonDetailsDTO;
import ro.tuc.ds2020.entities.Caregiver;

import static org.springframework.test.util.AssertionErrors.assertEquals;

import java.sql.Date;
import java.util.List;
import java.util.UUID;

@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:/test-sql/create.sql")
@Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = "classpath:/test-sql/delete.sql")
public class CaregiverServiceIntegrationTests extends Ds2020TestConfig {

    @Autowired
    CaregiverService caregiverService;

    @Test
    public void testGetCorrect() {
        List<CaregiverDTO> caregiverDTOList = caregiverService.findCaregivers();
        assertEquals("Test Insert Caregiver", 2, caregiverDTOList.size());
    }

    @Test
    public void testInsertCorrectWithGetById() {
        Date data=new Date(2000,11,11);
        CaregiverDTO p = new CaregiverDTO("Ioan",data,"Bistrita","male","caregiver@yahoo.com","pass");
        UUID insertedID = caregiverService.insert(p);

        CaregiverDTO insertedMedication = new CaregiverDTO(insertedID, p.getName(),p.getBirthDate(),p.getAddress(),p.getGender(),p.getEmail(),p.getPassword());
        CaregiverDTO fetchedMedication = caregiverService.findCaregiverById(insertedID);

        assertEquals("Test Inserted Medication", insertedMedication, fetchedMedication);
    }

    @Test
    public void testInsertCorrectWithGetAll() {
        Date data=new Date(2000,11,11);
        CaregiverDTO p = new CaregiverDTO("Ioan",data,"Bistrita","male","caregiver@yahoo.com","pass");
        caregiverService.insert(p);

        List<CaregiverDTO> caregiverDTOList = caregiverService.findCaregivers();
        assertEquals("Test Inserted Caregiver", 1, caregiverDTOList.size());
    }
}
