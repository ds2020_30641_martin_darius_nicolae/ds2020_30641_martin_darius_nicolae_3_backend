package ro.tuc.ds2020.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import ro.tuc.ds2020.Ds2020TestConfig;
import ro.tuc.ds2020.dtos.CaregiverDTO;
import ro.tuc.ds2020.dtos.MedicationDTO;
import ro.tuc.ds2020.dtos.PersonDetailsDTO;
import ro.tuc.ds2020.services.DoctorService;
import ro.tuc.ds2020.services.PersonService;

import java.sql.Date;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


public class CaregiverControllerUnitTest extends Ds2020TestConfig {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private DoctorService service;

    @Test
    public void insertCaregiverTest() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        Date data=new Date(2000,11,11);
        CaregiverDTO p = new CaregiverDTO("Ioan",data,"Bistrita","male","caregiver@yahoo.com","pass");

        mockMvc.perform(post("/caregiver/insert")
                .content(objectMapper.writeValueAsString(p))
                .contentType("application/json"))
                .andExpect(status().isCreated());
    }


}
